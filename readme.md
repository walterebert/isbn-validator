ISBN Validator
==============

(PHP 4, PHP5)

Validate a ISBN code

Description
-----------
    boolean we_validateIsbn ( string $isbn )

Validates an International Standard Book Number (ISBN) system code. Supported
are ISBN-10 and ISBN-13. It handles codes that contain hyphens or spaces.

Starting on January 1, 2007 ISBN-10 has been replaced by ISBN-13. Because some
publishers ran out of valid code before ISBN-13 was released, you can come
across books using invalid ISBN-10 codes.

Examples
--------

    require_once "isbn.php";
    
    // Don't make me think: A common sense approach to web usability (2nd edition)
    we_validateIsbn('0-321-34475-8'); // returns true
    
    // At Work: Neutelings Riedijk Architects
    we_validateIsbn('90-6450-508-X'); // returns true
    
    // At Work: Neutelings Riedijk Architects
    we_validateIsbn('978 90 6450 508 9'); // returns true


Copyright
---------
(c) 2003-2014 [Walter Ebert](http://walterebert.com/)

License
-------
[MIT](http://opensource.org/licenses/mit-license)

Reference
---------
* [ISBN](http://www.isbn-international.org/)
* [Wikipedia](http://en.wikipedia.org/wiki/ISBN)

