<?php
/**
 * Validate an International Standard Book Number (ISBN) system code. Supports
 * ISBN-10 and ISBN-13.
 *
 * PHP version 4 or later
 *
 * @author    Walter Ebert
 * @copyright 2003-2014 Walter Ebert
 * @license   http://opensource.org/licenses/mit-license MIT License
 * @version   2.1.3
 * @link      http://walterebert.com/
 * @param string $isbn ISBN code
 * @return boolean
 */

function we_validateIsbn($isbn)
{
    $isbn = trim((string) $isbn);
    $last = substr($isbn, -1);
    $stripped = preg_replace('/[^0-9]/', '', $isbn);
    $length = strlen($stripped);
    $sum = 0;

    if ($length == 9 and ($last == 'x' or $last == 'X')) {
        $stripped = $stripped . 'X';
        $length = 10;
    }

    // ISBN-10
    if ($length == 10) {
        for ($i = 0; $i < 10; $i++) {
            $value = substr($stripped, $i, 1);
            $value = ( $value == 'X' ) ? 10 : (int) $value;
            $sum += (10 - $i) * $value;
        }

        $remainder = $sum % 11;

        return empty($remainder);
    }
    
    // ISBN-13
    if ($length == 13) {
        for ($i = 0; $i < 12; $i++) {
            $j = $i % 2;
            $digit = (int) substr($stripped, $i, 1);

            if (empty($j)) {
                $sum += $digit;
            } else {
                $sum += 3 * $digit;
            }
        }

        $remainder = $sum % 10 ;
        $checkdigit = empty($remainder) ? 0 : 10 - $remainder ;
        $lastdigit = substr($stripped, -1, 1);

        return ($lastdigit == $checkdigit) ;
    }

    // Invalid
    return false;
}
