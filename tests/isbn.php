<?php
/**
 * @requires PHP 5.4
 */

class IsbnTest extends PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider isbnProvider
     */
    public function testIsbn($isbn, $expected)
    {
        $this->assertEquals($expected, we_validateIsbn($isbn));
    }

    public function isbnProvider()
    {
        return [
            ['0-321-34475-8', true],
            ['0-321-34475-7', false],
            ['90-6450-508-X', true],
            ['978 90 6450 508 9', true],
            [9789064505089, true],
            ['978 90 6450 508 8', false],
            ['abcdefghij', false],
            ['', false]
        ];
    }
}
